import React from 'react';
import Month  from '../components/Month/Month'

import '../styles/Calendar.css';

export default class CalendarContainer extends React.Component {
    render() {
        return (
            <div className="scrollable">
              <Month />
            </div>
        );
    }
}

