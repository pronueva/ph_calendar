import React from 'react';
import { styles , calendarDate } from './base'; //base del componente
import { getMonthUsedWeeks , getMonthWeeks } from '../../utils/getCalendarMonthWeeks';

import { Week } from './Weeks/Week'
import { Employees } from './Employees/Employees'
import { Grid , Loader , Dimmer , Image } from 'semantic-ui-react';

export class Month extends React.Component{
    constructor(){
        super();
        this.state = {
            currentMonthName: "", // nombre del mes actual
            currentMonthWeeks: [] , // arreglo de semanas del mes actual
            currentUsedWeeks: 0 // n° de semanas en las que está presente el mes actual
        }
    }
    componentDidMount(){
        let yearNumber = this.props.currentYear; // año
        let monthNumber = this.props.currentMonth; // numero del mes actual

        let firstDayOfMonth = new Date(yearNumber , monthNumber, 1).getDay(); // primer dia del mes
        let days = new Date(yearNumber , monthNumber+1 , 0 ).getDate() // numero de dias del mes
        let weeks = getMonthUsedWeeks(days , firstDayOfMonth);

        this.setState({
            currentMonthName: calendarDate.Months[monthNumber] ,
            currentMonthWeeks:  getMonthWeeks(  yearNumber ,  monthNumber , weeks ) ,
            currentUsedWeeks: weeks,
        })
    }
    weeks(){
        return this.state.currentMonthWeeks.map( week  => {
            return(
                <Grid.Column key={ week.id } className="wide week">
                    <Week days={ week.days } week={ week } month={ week.month } year={ week.year } />
                </Grid.Column>
            )
        })
    }
    employees(){
        return this.state.currentMonthWeeks.map( week  => {
            return(
                <Grid.Column key={ week.id } className="wide week">
                    <Employees  days={ week.days } week={ week } month={ week.month } year={ week.year } />
                </Grid.Column>
            )
        })
    }
    render(){
        const { department } = this.props;
        return(
            <div>
                <Grid centered divided columns={ this.state.currentUsedWeeks + 1 } className="month">
                    <Grid.Row centered>
                        <h1 style={styles.headingMonth} >{this.props.currentYear + ' ' + calendarDate.Months[this.props.currentMonth] }</h1>
                    </Grid.Row>
                    <Grid.Row className="week-row">
                        <div className="wide employee column week"></div>
                        { this.state.currentUsedWeeks > 0 ? this.weeks() : "" }
                    </Grid.Row>
                    { department.employees ?
                        department.employees.map( employee => {
                            return(
                                <Grid.Row className="week-row" key={employee.id}>
                                    <div className="wide employee column week">{employee.first_name + " " + employee.last_name}</div>
                                    { this.state.currentUsedWeeks > 0 ? this.employees() : "" }
                                </Grid.Row>
                            )
                        } )
                        :
                        <Grid.Row className="week-row">
                            <Dimmer active inverted>
                                <Loader content="Cargando datos ..."  />
                            </Dimmer>
                            <Grid.Row className="week-row">
                                <Image floated="left" src="https://react.semantic-ui.com/assets/images/wireframe/short-paragraph.png"  />
                            </Grid.Row>
                        </Grid.Row>
                    }
                </Grid>
            </div>
        )
    }
}
