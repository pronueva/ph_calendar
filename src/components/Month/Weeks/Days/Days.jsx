import React from 'react';
import { Base ,  styles , calendarDate  } from '../../base'; //base del componente

import { Grid } from 'semantic-ui-react';

export class Days extends Base{
    dayName( day ){
        return calendarDate.weekdaysMin[ day ]
    }
    render(){
        return(
            <Grid.Column style={styles.calendarDayGrid}>
                <p className={this.props.day.is_month_day} >{ this.dayName( this.props.day.day ) }</p>
                <p className="month number" >{ this.props.day.number }</p>
            </Grid.Column>
        )
    }
}