import React from 'react';
import { Base ,  styles } from '../base'; //base del componente

import { Days } from './Days/Days';

import { Grid } from 'semantic-ui-react';

export class Week extends Base{
    constructor(props){
        super(props);
        this.state = {
            days : []
        }
    }

    getWeekDays(){
        return this.props.days.map( day => {
            return(
                <Days day={ day } key={day.number} />
            )
        })
    }

    render(){
        return(
            <Grid columns="equal">
                { this.getWeekDays() }
                <Grid.Column style={styles.calendarDayGrid}>
                    <p className="month"></p>
                    <p className="month">+</p>
                </Grid.Column>
            </Grid>
        )
    }
}
