import React from 'react'

const es_date =  require('../../date_translations.json')["es"];

export const styles = {
    headingMonth : {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px",
        width: "calc(100%/8)"
    }
}
export const calendarDate = {
    Months : es_date["months"].split("_"),
    MonthsShort : es_date["monthsShort"].split("_"),
    WeekDays : es_date["weekdays"].split("_"),
    WeekdaysShort : es_date["weekdaysShort"].split("_"),
    weekdaysMin : es_date["weekdaysMin"].split("_")
}

export class Base extends React.Component{
    render(){
        return(
            <div></div>
        )
    }
}
