import React from 'react';

import { Days } from '../Weeks/Days/Days';

import { Grid , Dropdown } from 'semantic-ui-react';

const options = [
    { key: 1, text: '611', value: "1" },
    { key: 2, text: '612', value: "2"},
    { key: 3, text: '613', value: "3"},
]

export const styles = {
    headingMonth : {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px"
    }
}
export class Employees extends React.Component{
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    getWeekDays(){
        return this.props.days.map( day => {
            return(
                <Days day={ day } key={day.number} />
            )
        })
    }
    onChange(e,data){
        console.log(data);
    }

    render(){
        return(
            <Grid columns="equal">
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown item trigger={<span className="month">611</span>} icon={null} >
                        <Dropdown.Menu>
                            {
                                options.map(opt =>{
                                    return(
                                        <Dropdown.Item text={opt.text} key={opt.key} value={opt.value} onClick={ (e,data) => this.onChange(e,data) } />
                                    )
                                })
                            }
                        </Dropdown.Menu>
                    </Dropdown>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
                <Grid.Column style={styles.calendarDayGrid}>
                    <Dropdown trigger={<span className="month">611</span>} options={options} icon={null}/>
                </Grid.Column>
            </Grid>
        )
    }
}