import React from 'react';
import './index.css';
import CalendarContainer from './containers/CalendarContainer';


class CalendarTool extends React.Component {
    render() {
        if(!this.props.department_id){
            return (
                <h1>
                    ¡Debe seleccionar un departamento!
                </h1>
            )
        }
        return (
                <CalendarContainer
                date={ this.props.date ? this.props.date : new Date() }
                department_id={ this.props.department_id ? this.props.department_id : 2 }
                />
        )
    }
}
export default CalendarTool;
