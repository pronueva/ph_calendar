import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CalendarTool from './CalendarTool';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<CalendarTool />, document.getElementById('root'));
registerServiceWorker();
