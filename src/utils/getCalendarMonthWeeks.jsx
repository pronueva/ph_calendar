/**
 * Devuelve la cantidad de semanas en las que se encuentran los dias del mes
 * @params  monthDays , firstDayOfMonth
 * return integer
 */
export function getMonthUsedWeeks( monthDays , firstDayOfMonth ) {
    let MonthWeeks = 0;
    if( monthDays === 31 ){
        // 5 semanas si comienza de lunes a viernes
        // 6 semanas si comienza de sabado a domingo
        MonthWeeks = firstDayOfMonth > 0 && firstDayOfMonth < 6 ? 5 : 6
    }
    if( monthDays === 30 ){
        // 5 semanas si comienza de lunes a sabado
        // 6 semanas si comienza domingo
        MonthWeeks = firstDayOfMonth === 0  ? 6 : 5
    }
    if( monthDays === 29 ){
        // 5 semanas SIEMPRE
        MonthWeeks = 5
    }
    if( monthDays === 28 ){
        // 4 semanas si comienza lunes
        // 5 semanas si comienza de martes a domingo
        MonthWeeks = firstDayOfMonth === 1  ? 4 : 5
    }
    return MonthWeeks;
}
/**
 * Devuelve las semanas del mes con sus dias correspondientes
 * @params  years , month , weeks
 * return Object
 */
export function getMonthWeeks ( year ,  month , weeks ){
    /* CONTAR LAS SEMANAS EN LAS QUE ESTAN*/
    let firstDayOfMonth = new Date( year , month , 1 ).getDay(); // primer dia del mes

    const w = [];

    let  day = firstDayOfMonth === 1 ? new Date( year , month , 1 ) : new Date(new Date( year , month , 1 ) - (864e5 * ( firstDayOfMonth-1 )) )

    for(let i = 1; i <= weeks ; i ++ )
    {
        let weekFirstDay = day
        if ( i > 1 ) {
            let newDate = new Date( day )
            newDate.setDate( day.getDate() + ((i-1)*7) )
            weekFirstDay = newDate
        }
        //AGARRAR EL DOMINGO DE CADA SEMANA
        w[i] = {
            "id": i,
            "year": year,
            "month": month,
            "days" : getWeekDays( weekFirstDay , i , month )
        };
    }
    return w;
}
/**
 * Devuelve los dias de la semana correspondiente
 * @params  day , week , month
 * return Object
 */
function getWeekDays( day , week , month ){
    const d = [];
    let n = day;
    let dayN = 0;
    let newDate = new Date( day );
    for(let i = 1 ; i <= 7 ; i ++ ){
        n ++;
        dayN = i;

        if(i === 7 ){
            dayN = 0;
        }
        newDate =  new Date( day.valueOf() + (864e5*(i-1)) );

        let is_month_day = newDate.getMonth() === month ? "month day" : "no month day";

        d[i] = {
            "day": newDate.getDay(),
            "number": newDate.getDate(),
            "month": newDate.getMonth(),
            "is_month_day": is_month_day
        }
    }
    return d;
}