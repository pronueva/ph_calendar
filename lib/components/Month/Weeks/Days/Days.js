'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Days = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _base = require('../../base');

var _semanticUiReact = require('semantic-ui-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } //base del componente

var Days = exports.Days = function (_Base) {
    _inherits(Days, _Base);

    function Days() {
        _classCallCheck(this, Days);

        return _possibleConstructorReturn(this, (Days.__proto__ || Object.getPrototypeOf(Days)).apply(this, arguments));
    }

    _createClass(Days, [{
        key: 'dayName',
        value: function dayName(day) {
            return _base.calendarDate.weekdaysMin[day];
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _semanticUiReact.Grid.Column,
                { style: _base.styles.calendarDayGrid },
                _react2.default.createElement(
                    'p',
                    { className: this.props.day.is_month_day },
                    this.dayName(this.props.day.day)
                ),
                _react2.default.createElement(
                    'p',
                    { className: 'month number' },
                    this.props.day.number
                )
            );
        }
    }]);

    return Days;
}(_base.Base);