'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Base = exports.calendarDate = exports.styles = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var es_date = require('../../date_translations.json')["es"];

var styles = exports.styles = {
    headingMonth: {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px",
        width: "calc(100%/8)"
    }
};
var calendarDate = exports.calendarDate = {
    Months: es_date["months"].split("_"),
    MonthsShort: es_date["monthsShort"].split("_"),
    WeekDays: es_date["weekdays"].split("_"),
    WeekdaysShort: es_date["weekdaysShort"].split("_"),
    weekdaysMin: es_date["weekdaysMin"].split("_")
};

var Base = exports.Base = function (_React$Component) {
    _inherits(Base, _React$Component);

    function Base() {
        _classCallCheck(this, Base);

        return _possibleConstructorReturn(this, (Base.__proto__ || Object.getPrototypeOf(Base)).apply(this, arguments));
    }

    _createClass(Base, [{
        key: 'render',
        value: function render() {
            return _react2.default.createElement('div', null);
        }
    }]);

    return Base;
}(_react2.default.Component);