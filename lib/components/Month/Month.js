'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Month = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _base = require('./base');

var _getCalendarMonthWeeks = require('../../utils/getCalendarMonthWeeks');

var _Week = require('./Weeks/Week');

var _Employees = require('./Employees/Employees');

var _semanticUiReact = require('semantic-ui-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } //base del componente


var Month = exports.Month = function (_React$Component) {
    _inherits(Month, _React$Component);

    function Month() {
        _classCallCheck(this, Month);

        var _this = _possibleConstructorReturn(this, (Month.__proto__ || Object.getPrototypeOf(Month)).call(this));

        _this.state = {
            currentMonthName: "", // nombre del mes actual
            currentMonthWeeks: [], // arreglo de semanas del mes actual
            currentUsedWeeks: 0 // n° de semanas en las que está presente el mes actual
        };
        return _this;
    }

    _createClass(Month, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var yearNumber = this.props.currentYear; // año
            var monthNumber = this.props.currentMonth; // numero del mes actual

            var firstDayOfMonth = new Date(yearNumber, monthNumber, 1).getDay(); // primer dia del mes
            var days = new Date(yearNumber, monthNumber + 1, 0).getDate(); // numero de dias del mes
            var weeks = (0, _getCalendarMonthWeeks.getMonthUsedWeeks)(days, firstDayOfMonth);

            this.setState({
                currentMonthName: _base.calendarDate.Months[monthNumber],
                currentMonthWeeks: (0, _getCalendarMonthWeeks.getMonthWeeks)(yearNumber, monthNumber, weeks),
                currentUsedWeeks: weeks
            });
        }
    }, {
        key: 'weeks',
        value: function weeks() {
            return this.state.currentMonthWeeks.map(function (week) {
                return _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { key: week.id, className: 'wide week' },
                    _react2.default.createElement(_Week.Week, { days: week.days, week: week, month: week.month, year: week.year })
                );
            });
        }
    }, {
        key: 'employees',
        value: function employees() {
            return this.state.currentMonthWeeks.map(function (week) {
                return _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { key: week.id, className: 'wide week' },
                    _react2.default.createElement(_Employees.Employees, { days: week.days, week: week, month: week.month, year: week.year })
                );
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var department = this.props.department;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _semanticUiReact.Grid,
                    { centered: true, divided: true, columns: this.state.currentUsedWeeks + 1, className: 'month' },
                    _react2.default.createElement(
                        _semanticUiReact.Grid.Row,
                        { centered: true },
                        _react2.default.createElement(
                            'h1',
                            { style: _base.styles.headingMonth },
                            this.props.currentYear + ' ' + _base.calendarDate.Months[this.props.currentMonth]
                        )
                    ),
                    _react2.default.createElement(
                        _semanticUiReact.Grid.Row,
                        { className: 'week-row' },
                        _react2.default.createElement('div', { className: 'wide employee column week' }),
                        this.state.currentUsedWeeks > 0 ? this.weeks() : ""
                    ),
                    department.employees ? department.employees.map(function (employee) {
                        return _react2.default.createElement(
                            _semanticUiReact.Grid.Row,
                            { className: 'week-row', key: employee.id },
                            _react2.default.createElement(
                                'div',
                                { className: 'wide employee column week' },
                                employee.first_name + " " + employee.last_name
                            ),
                            _this2.state.currentUsedWeeks > 0 ? _this2.employees() : ""
                        );
                    }) : _react2.default.createElement(
                        _semanticUiReact.Grid.Row,
                        { className: 'week-row' },
                        _react2.default.createElement(
                            _semanticUiReact.Dimmer,
                            { active: true, inverted: true },
                            _react2.default.createElement(_semanticUiReact.Loader, { content: 'Cargando datos ...' })
                        ),
                        _react2.default.createElement(
                            _semanticUiReact.Grid.Row,
                            { className: 'week-row' },
                            _react2.default.createElement(_semanticUiReact.Image, { floated: 'left', src: 'https://react.semantic-ui.com/assets/images/wireframe/short-paragraph.png' })
                        )
                    )
                )
            );
        }
    }]);

    return Month;
}(_react2.default.Component);