'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Employees = exports.styles = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Days = require('../Weeks/Days/Days');

var _semanticUiReact = require('semantic-ui-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var options = [{ key: 1, text: '611', value: "1" }, { key: 2, text: '612', value: "2" }, { key: 3, text: '613', value: "3" }];

var styles = exports.styles = {
    headingMonth: {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px"
    }
};

var Employees = exports.Employees = function (_React$Component) {
    _inherits(Employees, _React$Component);

    function Employees(props) {
        _classCallCheck(this, Employees);

        var _this = _possibleConstructorReturn(this, (Employees.__proto__ || Object.getPrototypeOf(Employees)).call(this, props));

        _this.onChange = _this.onChange.bind(_this);
        return _this;
    }

    _createClass(Employees, [{
        key: 'getWeekDays',
        value: function getWeekDays() {
            return this.props.days.map(function (day) {
                return _react2.default.createElement(_Days.Days, { day: day, key: day.number });
            });
        }
    }, {
        key: 'onChange',
        value: function onChange(e, data) {
            console.log(data);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                _semanticUiReact.Grid,
                { columns: 'equal' },
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(
                        _semanticUiReact.Dropdown,
                        { item: true, trigger: _react2.default.createElement(
                                'span',
                                { className: 'month' },
                                '611'
                            ), icon: null },
                        _react2.default.createElement(
                            _semanticUiReact.Dropdown.Menu,
                            null,
                            options.map(function (opt) {
                                return _react2.default.createElement(_semanticUiReact.Dropdown.Item, { text: opt.text, key: opt.key, value: opt.value, onClick: function onClick(e, data) {
                                        return _this2.onChange(e, data);
                                    } });
                            })
                        )
                    )
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                ),
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { style: styles.calendarDayGrid },
                    _react2.default.createElement(_semanticUiReact.Dropdown, { trigger: _react2.default.createElement(
                            'span',
                            { className: 'month' },
                            '611'
                        ), options: options, icon: null })
                )
            );
        }
    }]);

    return Employees;
}(_react2.default.Component);