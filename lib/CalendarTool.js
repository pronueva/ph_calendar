'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

require('./index.css');

var _CalendarContainer = require('./containers/CalendarContainer');

var _CalendarContainer2 = _interopRequireDefault(_CalendarContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CalendarTool = function (_React$Component) {
    _inherits(CalendarTool, _React$Component);

    function CalendarTool() {
        _classCallCheck(this, CalendarTool);

        return _possibleConstructorReturn(this, (CalendarTool.__proto__ || Object.getPrototypeOf(CalendarTool)).apply(this, arguments));
    }

    _createClass(CalendarTool, [{
        key: 'render',
        value: function render() {
            if (!this.props.department_id) {
                return _react2.default.createElement(
                    'h1',
                    null,
                    '\xA1Debe seleccionar un departamento!'
                );
            }
            return _react2.default.createElement(_CalendarContainer2.default, {
                date: this.props.date ? this.props.date : new Date(),
                department_id: this.props.department_id ? this.props.department_id : 2
            });
        }
    }]);

    return CalendarTool;
}(_react2.default.Component);

exports.default = CalendarTool;