# Calendar for work shifts and assignments

## Folder Structure:

```
ph-calendar/
  README.md
  package.json
  node_modules/
  public/
    index.html
  src/
    components/
    containers/
    styles/
    images/
    utils/
    index.js
```
## props

year  | integer => "2017","2016" ...
month | integer => "0","1","2","3" ... "11"
day   | integer => "0","1","2","3" ... "31" (depende del mes)
